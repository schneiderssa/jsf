import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.cursojsf2.model.Pessoa;
import br.com.cursojsf2.util.HibernateUtil;

public class TesteHibernate {
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		/*classe para se usar com hibernate, session!*/
		Session session = HibernateUtil.getSession();
		
		
		/*Buscando na base de dados as pessoas cadastras da tabela pessoa e armazenando
		 * numa lista*/
		
		/*Objeto criteria, o nome j� diz! Crit�rios !*/
		List<Pessoa> pessoas = session.createCriteria(Pessoa.class)
				//gt -> maior que o c�digo 3
				.add(Restrictions.gt("codigo", 3))
				.list();
		
		for(Pessoa pessoa: pessoas) {
			System.out.println(pessoa.getCodigo() + " - " +pessoa.getNome());
		}
		
		
		
		
		
		session.close();
	}
}
