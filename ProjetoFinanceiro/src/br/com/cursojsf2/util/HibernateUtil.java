package br.com.cursojsf2.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			/*Serve para buscar as configurações do hibernate.cfg.xml*/
			Configuration cfg = new Configuration().configure();
			
			/*Serve para iniciar um contrutor de serviço do hibernate*/
			ServiceRegistry serviceRegistry =  new ServiceRegistryBuilder()
					.applySettings(cfg.getProperties()).buildServiceRegistry();
			
			sessionFactory = cfg.buildSessionFactory(serviceRegistry);
		} catch (Throwable e) {
			throw new ExceptionInInitializerError(e);
		}	
	}
	
	public static Session getSession() {
		return sessionFactory.openSession();
	}
}
