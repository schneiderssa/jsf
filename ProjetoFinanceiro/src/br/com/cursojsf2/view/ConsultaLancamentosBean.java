package br.com.cursojsf2.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import br.com.cursojsf2.model.Lancamento;
import br.com.cursojsf2.util.FacesUtil;
import br.com.cursojsf2.util.HibernateUtil;

@ManagedBean
public class ConsultaLancamentosBean {
	
	private List<Lancamento>lancamentos = new ArrayList<>();
	
	private Lancamento lancamentoSelecionado = new Lancamento();

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}
	
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void inicializar() {
		
		Session session = HibernateUtil.getSession();
		
		this.lancamentos = session.createCriteria(Lancamento.class)
				.addOrder(Order.asc("dataPagamento"))
				.list();
		
		
		session.close();
		
		
	}
	
	public void excluir() {
		if(this.lancamentoSelecionado.isPago()) {
			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_ERROR, "Lan�amento j� foi pago e n�o pode ser exclu�do");
		}else {
			Session session = HibernateUtil.getSession();
			Transaction trx = session.beginTransaction();
			session.delete(this.lancamentoSelecionado);
			trx.commit();
			session.close();
			
			this.inicializar();
			
			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_INFO, "Lan�amento exclu�do com sucesso");
			
			
		}
		
	}
	public Lancamento getLancamentoSelecionado() {
		return lancamentoSelecionado;
	}


	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}

}
