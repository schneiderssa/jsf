package br.com.cursojsf2.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

import br.com.cursojsf2.model.Lancamento;
import br.com.cursojsf2.model.Pessoa;
import br.com.cursojsf2.model.TipoLancamento;
import br.com.cursojsf2.util.HibernateUtil;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class CadastroLancamentoBean implements Serializable{
	
	private Lancamento lancamento = new Lancamento();
	
	private List<Pessoa> pessoas = new ArrayList<Pessoa>();
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		
		Session session = HibernateUtil.getSession();
		
		this.pessoas = session.createCriteria(Pessoa.class)
				.addOrder(Order.asc("nome"))
				.list();
		session.close();
	}
	
	public TipoLancamento[] getTipoLancamentos() {
		return TipoLancamento.values();
	}
	
	public Lancamento getLancamento() {
		return lancamento;
	}
	
	public List<Pessoa> getPessoas(){
		return pessoas;
	}
	
	public void lancamentoPagoModificado(ValueChangeEvent event) {
		this.lancamento.setPago((Boolean)event.getNewValue());
		
	}
	
	public void cadastrar() {
		Session session = HibernateUtil.getSession();
		/*Iniciando uma transa��o com o banco de dados*/
		Transaction trx = session.beginTransaction();
		
		/*merge serve tanto para atualizar o objeto j� existente ou para cadastrar
		 * se caso n�o existir*/
		session.merge(this.lancamento);
		
		trx.commit();
		session.close();
		
		this.lancamento = new Lancamento();
		
		String msg = "Cadastro efetuado com sucesso";
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,msg,msg));
	}
}
